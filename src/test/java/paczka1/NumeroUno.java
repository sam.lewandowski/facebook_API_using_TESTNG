package paczka1;

import com.restfb.*;
import com.restfb.types.FacebookType;
import com.restfb.types.Post;
import com.restfb.types.User;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.util.List;


public class NumeroUno {

    private FacebookClient fbClient;

    @BeforeTest
    public void establishConnection() {
        String authToken = "EAAEEaGCzUYMBAPEYZBe0j7NU9SyZBZCsPnTZBr83XRSvqxT8aFIzR5eTABcv4PwhwDUr1QQZAG34abeYtxgQtyJd8SZAa8Tzp5OUGZBdZBYCLHjZBUxdpYaAL8EsjkGjO9FOf4DFYcpZAY0FV6sfRjVHEljcExPO96kFM8UZBHy1ABXsEkvwDVDBMXL";
        fbClient = new DefaultFacebookClient(authToken, Version.VERSION_2_8);
        User me = fbClient.fetchObject("me", User.class);

        Assert.assertNotNull(me.getName());
    }

    @Test ()
    public void publishCheckDeletePost(){
        String postID;

        //Step 1: Publish Post
        FacebookType response = getResponse("Pioter dej mnie kebaba! ");
        postID = response.getId();
        Assert.assertNotNull(postID);

        //Step 2: Check Post
        checkPost(getResult(),getResponse("Z ostrym sosem! "));

        //Step 3: Delete Post
        checkPost(getResult(),getResponse("Halyna zrob mnie kebaba! "));
        fbClient.deleteObject(response.getId()); //usuniecie Id z obiektu powoduje usuniecie całego obiektu.
    }

    private FacebookType getResponse(String msg){
        FacebookType response = fbClient.publish("me/feed", FacebookType.class, Parameter.with("message", msg));
        System.out.println("fb.com/"+response.getId());
        return response;
    }

    private Connection<Post> getResult(){
        return fbClient.fetchConnection("me/feed",Post.class);
    }

    private void checkPost(Connection<Post> result, FacebookType response){
        for(List<Post> page : result){
            for(Post aPost : page){
                Assert.assertNotEquals(aPost.getId(),response.getId());
            }
        }
    }
}